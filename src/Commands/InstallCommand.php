<?php

namespace VendorName\Skeleton\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputOption;

class InstallCommand extends Command
{
     /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = ':vendor_name::package_name:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install :package_name files';


    public function __construct(Filesystem $files)
    {
        parent::__construct();

    }


    public function fire()
    {
        return $this->handle();
    }


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        
    }
}
