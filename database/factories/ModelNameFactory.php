<?php

namespace VendorName\Skeleton\Database\Factories;

use VendorName\Skeleton\Models\ModelName;
use Illuminate\Database\Eloquent\Factories\Factory;

class ModelNameFactory extends Factory
{
    protected $model = ModelName::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
