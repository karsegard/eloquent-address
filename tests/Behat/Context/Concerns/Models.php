<?php

namespace EspaceTerroir\Tests\Behat\Context\Concerns;

use Behat\Gherkin\Node\PyStringNode;

/**
 * Defines application features from the specific context.
 */
trait Models 
{

    protected $model_attributes = [];
    protected $model_record = null;
    protected $model_records = null;
    protected $model_query = null;
    protected $model_class = null;

    /**
     * @Given the model class is :arg1
     */
    public function theModelClassIs($arg1)
    {
        $this->model_class = $arg1;
    }

 /**
     * @Then we have :count records of model :class
     */
    public function weHaveRecordsOfModel($count, $class)
    {
        $this->model_records = $class::all();
        $this->assertEquals($this->model_records->count(),$count);
    }

    /**
     * @Given a query on :class
     */
    public function aQueryOn($class)
    {
        $this->model_query = $class::query();
    }

    /**
     * @Given with scope :scope with :args
     */
    public function withScopeWith($scope, $args)
    {
        $args=  explode(',',$args);
       
        if(is_string($args)){
            $args = [$args];
        }
        $this->model_query = call_user_func_array([$this->model_query,$scope],$args);
    }
    /**
     * @Then There is :count query results
     */
    public function thereIsQueryResults($count)
    {
        $this->assertEquals($count,$this->model_query->count());
    }
}
